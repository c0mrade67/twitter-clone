import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';
import { BackButton } from '../components/BackButton';



// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'Example/Button',
  component: BackButton,
  // More on argTypes: https://storybook.js.org/docs/react/api/argtypes
  argTypes: {
    backgroundColor: { control: 'color' },
  },
} as ComponentMeta<typeof BackButton>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: ComponentStory<typeof BackButton> = (args) => <BackButton {...args} />;

export const Primary = Template.bind({});
// More on args: https://storybook.js.org/docs/react/writing-stories/args
Primary.args = {

};

export const Secondary = Template.bind({});
Secondary.args = {

};

export const Large = Template.bind({});
Large.args = {

};

export const Small = Template.bind({});
Small.args = {

};
